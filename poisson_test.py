#!/usr/bin/python2

from __future__ import division
from pylab import *

kmax = 40

k = range(kmax)

l1 = 1

l2 = 10

def poisson(k, l):
    p = exp(-l)
    for m in range(1,k+1):
        p *= l / m        
    
    return p

p1 = zeros(kmax)
p2 = zeros(kmax)

for i in k:
    p1[i] = poisson(i, l1)
    p2[i] = poisson(i, l2)

def threshold_est(l1, l2):
    sum1 = 0
    sum2 = 0
    for k in range(l2):
        sum1 += poisson(k, l1)
        sum2 += poisson(k, l2)
        if sum1 + sum2 > 1:
            print("Threshold should be at %.1f." %(k+0.5))
            print("Fidelity limit roughly %f" %((sum2+1-sum1)/2) )
            break
            
    return k-0.5

threshold = threshold_est(l1, l2)

plot(k,p1,'r',k,p2,'b',[threshold, threshold], [0,0.3],'g')
grid('on')
show()
